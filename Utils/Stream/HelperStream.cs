using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ZifLib.Utils.Collection;
using ZifLib.Utils.StructureAndByteArray;

namespace ZifLib.Utils.Stream
{
	using Stream = System.IO.Stream;

	public static class HelperStream
	{
		public static Stream ToStream(this string s, Encoding encoding = null) =>
			s == null ? null : new MemoryStream((encoding ?? Encoding.UTF8).GetBytes(s));

		public static void Write<T>(this Stream stream, T buffer) where T : struct
		{
			stream.Write(buffer.ToByteArray());
		}

		public static void Write(this Stream stream, byte[] buffer)
		{
			stream.Write(buffer, 0, buffer.Length);
		}

		public static List<byte> ReadList(this Stream stream, int count = 1 << 16)
		{
			if (stream.Position >= stream.Length) return null;
			var buffer = new byte[count].ToList();
			var n = stream.Read(buffer.RawBuffer(), 0, count);
			if (n < 0) return null;
			if (count != n) buffer.RemoveRange(n, count - n);
			return buffer;
		}

		public static string ReadAll(this StreamReader stream)
		{
			using (stream)
			{
				return stream.ReadToEnd();
			}
		}

		public static byte[] ReadAll(this Stream stream)
		{
			using (stream)
			{
				return stream.Read(stream.Length);
			}
		}

		public static object Read(this Stream ar, Type t)
		{
			return ar.Read(t.SizeStruct()).ToStruct(t);
		}

		public static T? Read<T>(this Stream ar) where T : struct
		{
			return ar.Read(typeof(T)) as T?;
		}

		public static byte[] Read(this Stream stream, long count = 1 << 16)
		{
			var buffer = new byte[count];
			var n = stream.Read(buffer, 0, (int) count);
			return n >= 0 ? buffer.NewSize(n) : null;
		}
	}
}