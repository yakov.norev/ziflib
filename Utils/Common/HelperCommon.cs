using System;

namespace ZifLib.Utils.Common
{
	public static class HelperCommon
	{
		public static T Ё<T>(this T t, Action<T> a)
		{
			a(t);
			return t;
		}

		public static bool OnNull<T>(this object o, Action<T> d) where T : class
		{
			var t = o as T;
			if (t == null) return false;
			d(t);
			return true;
		}

		public static T Lock<T, T1>(this T1 t, Func<T1, T> f) where T1 : class
		{
			lock (t) return f(t);
		}

		public static T Lock<T, T1>(this T1 t, Func<T> f) where T1 : class
		{
			lock (t) return f();
		}

		public static void Lock<T1>(this T1 t, Action f) where T1 : class
		{
			lock (t)
			{
				f();
			}
		}

		public static void Lock<T1>(this T1 t, Action<T1> f) where T1 : class
		{
			lock (t)
			{
				f(t);
			}
		}

		public static T Using<T, T1>(this T1 t, Func<T> f) where T1 : IDisposable
		{
			using (t)
			{
				return f();
			}
		}

		public static T Using<T, T1>(this T1 t, Func<T1, T> f) where T1 : IDisposable
		{
			using (t)
			{
				return f(t);
			}
		}

		public static void Using<T1>(this T1 t, Action f) where T1 : IDisposable
		{
			using (t)
			{
				f();
			}
		}

		public static void Using<T1>(this T1 t, Action<T1> f) where T1 : IDisposable
		{
			using (t)
			{
				f(t);
			}
		}
	}
}
