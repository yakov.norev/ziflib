﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ZifLib.Utils
{
	public class UtilsSerialize
	{
		public static T f<T>(string filesFN, Func<T> Fdef)
		{
			if (File.Exists(filesFN))
			{
				T t = default(T);
				if (Safe.Ab(() => { using (var f = File.OpenText(filesFN)) { t = (T)new XmlSerializer(typeof(T)).Deserialize(f); } }))
					return t;
			}
			var def = Fdef();
			Safe.Ab(() => { using (var f = File.CreateText(filesFN)) new XmlSerializer(typeof(T)).Serialize(f, def); });
			return def;
		}
	}
}
