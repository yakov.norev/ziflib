using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ZifLib.Utils
{
	public static class UtilsTasks
	{
		public static Task<T> Wait<T>(TimeSpan t, T a)
		{
			return new Task<T>(() =>
			{
				Thread.Sleep(t);
				return a;
			});
		}

		public static Task<T> WaitAll<T>(this IEnumerable<Task<T>> d, Func<T[], T> action)
		{
			return Task.Run(() => action(WaitAll_ar(d)));
		}

		private static T[] WaitAll_ar<T>(this IEnumerable<Task<T>> _d)
		{
			var d = _d.ToArray();
			Task.WaitAll(d);
			return Array.ConvertAll(d, _ => _.Result);
		}

		public static Task<T> WaitAny<T>(this Task<T>[] d)
		{
			return d[Task.WaitAny(d)];
		}

		public static Task WaitAny(this Task[] d)
		{
			return d[Task.WaitAny(d)];
		}

		public static Task Next<T>(this Task<T> task, Action<T> a)
		{
			return task.ContinueWith(_ => a(_.Result));
		}

		public static Task<T1> Next<T, T1>(this Task<T> task, Func<T, T1> a)
		{
			return task.ContinueWith(_ => a(_.Result));
		}
	}
}