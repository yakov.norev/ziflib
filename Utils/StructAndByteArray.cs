using System;
using System.Runtime.InteropServices;

namespace ZifLib.Utils
{
	public static class StructAndByteArray
	{
		public static byte[] StructureToByteArray(object obj)
		{
			var type = obj.GetType();
			var outputType = type.IsEnum ? Enum.GetUnderlyingType(type) : type;
			Convert.ChangeType(obj, outputType);
			return Bytes(obj);
		}

		public static T? ByteArrayToStructure<T>(byte[] ar) where T : struct
		{
			return ByteArrayToStructure(ar, typeof(T)) as T?;
		}

		public static object ByteArrayToStructure(byte[] ar, Type type)
		{
			return Obj(ar, type.IsEnum ? Enum.GetUnderlyingType(type) : type);
		}

		private static byte[] Bytes(object obj, Type type = null)
		{
			int len = Marshal.SizeOf(type ?? obj.GetType());
			byte[] arr = new byte[len];
			IntPtr ptr = Marshal.AllocHGlobal(len);
			Marshal.StructureToPtr(obj, ptr, true);
			Marshal.Copy(ptr, arr, 0, len);
			Marshal.FreeHGlobal(ptr);
			return arr;
		}

		private static object Obj(byte[] ar, Type type)
		{
			var length = Marshal.SizeOf(type);
			if (ar.Length < length) return null;
			var hGlobal = Marshal.AllocHGlobal(length);
			Marshal.Copy(ar, 0, hGlobal, length);
			var obj = Marshal.PtrToStructure(hGlobal, type);
			Marshal.FreeHGlobal(hGlobal);
			return obj;
		}
	}
}