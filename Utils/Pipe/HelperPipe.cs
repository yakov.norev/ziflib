using System.IO.Pipes;

namespace ZifLib.Utils.Pipe
{
	public static class HelperPipe
	{
		public static AnonymousPipeClientStream Client(this AnonymousPipeServerStream s)
		{
			return new AnonymousPipeClientStream(s.GetClientHandleAsString());
		}
	}
}
