using System;
using System.Runtime.InteropServices;

namespace ZifLib.Utils.StructureAndByteArray
{
	public static class HelperStructureAndByteArray
	{
		public static byte[] ToByteArray<T>(this T obj) where T : struct
		{
			return StructAndByteArray.StructureToByteArray(obj);
		}

		public static object ToStruct(this byte[] ar, Type t)
		{
			return StructAndByteArray.ByteArrayToStructure(ar, t);
		}

		public static T? ToStruct<T>(this byte[] ar) where T : struct
		{
			return StructAndByteArray.ByteArrayToStructure<T>(ar);
		}
		public static int SizeStruct(this Type type)
		{
			return Marshal.SizeOf(type.IsEnum ? Enum.GetUnderlyingType(type) : type);
		}
	}
}