﻿using System;
using System.Collections.Generic;

namespace ZifLib.Utils.Collection
{
	public static class HelperIDictionary
	{
		private static readonly object nullObj = new object();

		public static TValue Get<TValue, TKey>(this IDictionary<TKey, TValue> dic, TKey key, TValue def = default(TValue))
		{
			return dic.TryGetValue(key, out var v) ? v : def;
		}

		public static TValue? GetNulleble<TValue, TKey>(this IDictionary<TKey, TValue> dic, TKey key)
			where TValue : struct
		{
			return dic.TryGetValue(key, out var v) ? v : default(TValue?);
		}

		public static TValue GetKeyNull<TValue>(this IDictionary<object, TValue> dic, object key, TValue def = default(TValue))
		{
			return dic.Get(key ?? nullObj, def);
		}

		public static TValue CreateKeyNull<TValue>(this IDictionary<object, TValue> dic, object key) where TValue : new()
		{
			return dic.Create(key ?? nullObj, () => new TValue());
		}

		public static TValue CreateKeyNull<TValue>(this IDictionary<object, TValue> dic, object key, Func<TValue> creator)
		{
			return dic.Create(key ?? nullObj, () => creator());
		}

		public static TValue CreateKeyNull<TValue>(this IDictionary<object, TValue> dic, object key, Func<object, TValue> creator)
		{
			return dic.Create(key ?? nullObj, () => creator(key));
		}

		public static TValue Create<TValue, TKey>(this IDictionary<TKey, TValue> dic, TKey key)
			where TValue : new()
		{
			return dic.Create(key, () => new TValue());
		}

		public static TValue Create<TValue, TKey>(this IDictionary<TKey, TValue> dic, TKey key, Func<TValue> creator)
		{
			return dic.TryGetValue(key, out var v) ? v : dic[key] = creator();
		}

		public static TValue Create<TValue, TKey>(this IDictionary<TKey, TValue> dic, TKey key, Func<TKey, TValue> creator)
		{
			return dic.TryGetValue(key, out var v) ? v : dic[key] = creator(key);
		}
	}
}
