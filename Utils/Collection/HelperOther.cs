﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ZifLib.Utils.Collection
{
	public static class HelperOther
	{
		public static T[] NewSize<T>(this T[] a, int size)
		{
			if (size == a.Length) return a;
			var _ = new T[size];
			Array.Copy(a, _, Math.Min(size, a.Length));
			return _;
		}

		public static T[] RawBuffer<T>(this List<T> l)
		{
			return l.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(l) as T[];
		}
	}
}
