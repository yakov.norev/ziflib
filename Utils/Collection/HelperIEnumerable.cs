﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZifLib.Utils.Collection
{
	public static class HelperIEnumerable
	{
		public static T? FirstNull<T>(this IEnumerable<T> e)
			where T : struct
		{
			using (var ie = e.GetEnumerator())
			{
				if (!ie.MoveNext()) return null;
				return ie.Current;
			}
		}

		public static string JoinStr<T>(this IEnumerable<T> e, string sepr)
		{
			using (var ie = e.GetEnumerator())
			{
				var sb = new StringBuilder();
				if (!ie.MoveNext()) return sb.ToString();
				sb.Append(ie.Current.ToString());
				while (ie.MoveNext()) sb.Append(sepr).Append(ie.Current.ToString());
				return sb.ToString();
			}
		}

		public static T Max<T>(this IEnumerable<T> e, Comparison<T> a)
		{
			using (var ie = e.GetEnumerator())
			{
				if (!ie.MoveNext()) throw new Exception();
				T x = ie.Current;
				while (ie.MoveNext())
				{
					var item = ie.Current;
					if (a(x, item) < 0) x = item;
				}

				return x;
			}
		}

		public static T Min<T>(this IEnumerable<T> e, Comparison<T> a)
		{
			using (var ie = e.GetEnumerator())
			{
				if (!ie.MoveNext()) throw new Exception();
				T x = ie.Current;
				while (ie.MoveNext())
				{
					var item = ie.Current;
					if (a(x, item) > 0) x = item;
				}

				return x;
			}
		}

		public static bool All(this IEnumerable<bool> d)
		{
			return d.All(_ => _);
		}

		public static bool Any(this IEnumerable<bool> d)
		{
			return d.Any(_ => _);
		}

		public static IEnumerable<T> For<T>(this IEnumerable<T> e, Action<T> a)
		{
			foreach (var item in e) a(item);
			return e;
		}

		public static IEnumerable<T> For<T>(this IEnumerable<T> e, Action<T, int> a)
		{
			var i = 0;
			using (var g = e.GetEnumerator())
				while (g.MoveNext())
					a(g.Current, i++);
			return e;
		}

		public static IEnumerable<T> For<T, T1>(this IEnumerable<T> e, Action<T1, T> a, T1 b)
		{
			foreach (var item in e) a(b, item);
			return e;
		}

		public static IEnumerable<T> For<T, T1>(this IEnumerable<T> e, Action<T1, T, int> a, T1 b)
		{
			var i = 0;
			using (var g = e.GetEnumerator())
				while (g.MoveNext())
					a(b, g.Current, i++);
			return e;
		}

		public static IEnumerable<T> WithPrev<T>(this IEnumerable<T> e, Action<T, T> a)
		{
			using (var g = e.GetEnumerator())
			{
				if (!g.MoveNext()) return e;
				var old = g.Current;
				while (g.MoveNext())
				{
					var current = g.Current;
					a(current, old);
					old = current;
				}
			}

			return e;
		}

		public static IEnumerable<T> WithPrev<T>(this IEnumerable<T> e, Action<T, T, int> a)
		{
			var i = 0;
			using (var g = e.GetEnumerator())
			{
				if (!g.MoveNext()) return e;
				var old = g.Current;
				while (g.MoveNext())
				{
					var current = g.Current;
					a(current, old, i++);
					old = current;
				}
			}

			return e;
		}

		public static IEnumerable<T1> WithPrev<T, T1>(this IEnumerable<T> e, Func<T, T, T1> a)
		{
			using (var g = e.GetEnumerator())
			{
				if (!g.MoveNext()) yield break;
				var old = g.Current;
				while (g.MoveNext())
				{
					var current = g.Current;
					yield return a(current, old);
					old = current;
				}
			}

			yield break;
		}

		public static IEnumerable<T1> WithPrev<T, T1>(this IEnumerable<T> e, Func<T, T, int, T1> a)
		{
			var i = 0;
			using (var g = e.GetEnumerator())
			{
				if (!g.MoveNext()) yield break;
				var old = g.Current;
				while (g.MoveNext())
				{
					var current = g.Current;
					yield return a(current, old, i++);
					old = current;
				}
			}

			yield break;
		}
	}
}
