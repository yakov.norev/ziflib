﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZifLib.Utils.Collection
{
	public static class HelperIList
	{
		public static T AddS<T>(this ICollection<T> coll, T x)
		{
			coll.Add(x);
			return x;
		}

		public static ICollection<T> AddThis<T>(this ICollection<T> coll, T x)
		{
			coll.Add(x);
			return coll;
		}

		public static void AddRange<T>(this ICollection<T> coll, IEnumerable<T> x)
		{
			foreach (var item in x)
			{
				coll.Add(item);
			}
		}

		public static ICollection<T> AddRangeThis<T>(this ICollection<T> coll, IEnumerable<T> x)
		{
			foreach (var item in x)
			{
				coll.Add(item);
			}
			return coll;
		}

		public static ICollection<T> CopyR<T>(this ICollection<T> a)
		{
			T[] x = new T[a.Count];
			a.CopyTo(x, 0);
			return x;
		}

		public static IList<T> CopyRW<T>(this ICollection<T> a)
		{
			List<T> x = new List<T>(a);
			return x;
		}

		public static T GetSafe<T>(this IList<T> t, int i, T def = default(T))
		{
			return i < t.Count && i >= 0 ? t[i] : def;
		}

		public static T GetSafe<T>(this IList<T> t, int i, Func<T> creator)
		{
			return i < t.Count && i >= 0 ? t[i] : creator();
		}

		public static T GetSafe<T>(this IList<T> t, int i, Func<int, T> creator)
		{
			return i < t.Count && i >= 0 ? t[i] : creator(i);
		}

		public static T GetСycle<T>(this IList<T> t, int i)
		{
			if (t.Count == 0) throw new IndexOutOfRangeException();
			if (i < 0)
			{
				return t[(t.Count + (i % t.Count)) % t.Count];
			}
			else
			{
				return t[i % t.Count];
			}
		}

		public static T FastDel<T>(this IList<T> t, int i)
		{
			var foo = t[i];
			t.RemoveFast(i);
			return foo;
		}

		public static int FastDelCount<T>(this IList<T> ps, Predicate<T> predicate)
		{
			var Num = 0;
			for (var index = 0; index < ps.Count;)
			{
				if (!predicate(ps[index]))
				{
					index++;
					continue;
				}

				Num++;
				ps.FastDel(index);
			}

			return Num;
		}

		public static IList<T> FastDelRet<T>(this IList<T> ps, Predicate<T> predicate)
		{
			var ret = new List<T>();
			for (var index = 0; index < ps.Count;)
			{
				if (!predicate(ps[index]))
				{
					index++;
					continue;
				}

				ret.Add(ps.FastDel(index));
			}

			return ret;
		}

		public static void RemoveFast<T>(this IList<T> li, int index)
		{
			if (index != li.Count - 1) li[index] = li[li.Count - 1];
			li.RemoveAt(li.Count - 1);
		}

		public static void InsertFast<T>(this IList<T> li, int index, T item)
		{
			if (li.Count == index)
			{
				li.Add(item);
			}
			else
			{
				li.Add(li[index]);
				li[index] = item;
			}
		}

		public static T Rnd<T>(this IList<T> list, Random rnd = null)
		{
			rnd = rnd ?? new Random();
			return list[rnd.Next(list.Count)];
		}
	}

	namespace JSFast
	{
		public static class HelperIListJs
		{
			public static string JsStr<T>(this IList<T> t)
			{
				return '[' + string.Join(",", t.Select(a => a.ToString())) + ']';
			}
		}
	}
}
