﻿using System.Linq;
using System.Reflection;

namespace ZifLib.Utils.Reflection
{
    public static class HelperReflection
    {
        public static T GetField<T>(this object a, string name) where T : class
        {
            var type = a.GetType();
            var fieldInfo = type.GetField(name, BindingFlags.NonPublic);
            return fieldInfo.GetValue(a) as T;
        }
        public static T GetProperty<T>(this object a, string name) where T : class
        {
            var type = a.GetType();
            var propertyInfo = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance)
                .FirstOrDefault(x => x.Name == name);
            return propertyInfo.GetValue(a) as T;
        }
    }
}
