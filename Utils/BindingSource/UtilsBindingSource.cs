﻿using System.Collections.Generic;

namespace ZifLib.Utils.BindingSource
{
	public static class Helper
	{
		public static void AddRange(this System.Windows.Forms.BindingSource bindingSource, IEnumerable<object> objs)
		{
			if (objs != null) foreach (var item in objs) bindingSource.Add(item);
		}
	}
}
