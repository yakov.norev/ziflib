﻿using System;
using System.Collections.Generic;

namespace ZifLib.Utils.RandomUtils
{
	public static class HelperRandom
	{
		private static bool haveNextNextGaussian;
		private static double nextNextGaussian;

		public static double NextGaussian(this Random rand)
		{
			if (haveNextNextGaussian)
			{
				haveNextNextGaussian = false;
				return nextNextGaussian;
			}
			else
			{
				double v1, v2, s;
				do
				{
					v1 = 2 * rand.NextDouble() - 1;   // between -1.0 and 1.0
					v2 = 2 * rand.NextDouble() - 1;   // between -1.0 and 1.0
					s = v1 * v1 + v2 * v2;
				} while (s >= 1 || s == 0);
				double multiplier = Math.Sqrt(-2 * Math.Log(s) / s);
				nextNextGaussian = v2 * multiplier;
				haveNextNextGaussian = true;
				return v1 * multiplier;
			}
		}
		public static T Next<T>(this Random rnd, IList<T> list) => list[rnd.Next(list.Count)];
	}
}
