using System.Linq;
using System;

namespace ZifLib.Utils
{
	public static class UtilsList
	{
		public static T[] CArray<T>(int num, Func<int, T> a) => Enumerable.Range(0, num).Select(a).ToArray();

		public static string Sub(this string str, int pos, int len)
		{
			var l = str.Length;
			var p = Math.Min(pos, l);
			p = p < 0 ? l + p : p;
			return str.Substring(Math.Max(0, p), Math.Max(Math.Min(p + len, l) - p, 0));
		}
	}
}