﻿using System;

namespace ZifLib.Utils.Delegate
{
	public static class HelperDelegate
	{
		public static Action DG(Action a)
		{
			return a;
		}
		public static Action<T> DG<T>(Action<T> a)
		{
			return a;
		}

		public static Action<T, T1> DG<T, T1>(Action<T, T1> a)
		{
			return a;
		}

		public static Action<T, T1, T2> DG<T, T1, T2>(Action<T, T1, T2> a)
		{
			return a;
		}

		public static Func<T> DG<T>(Func<T> a)
		{
			return a;
		}

		public static Func<T, T1> DG<T, T1>(Func<T, T1> a)
		{
			return a;
		}

		public static Func<T, T1, T2> DG<T, T1, T2>(Func<T, T1, T2> a)
		{
			return a;
		}

		public static void WNulleble<T>(this Action<T> a, T? b) where T : struct
		{
			if (b.HasValue) a(b.Value);
		}

		public static T1 WNulleble<T, T1>(this Func<T, T1> a, T? b) where T : struct
		{
			return b.HasValue ? a(b.Value) : default(T1);
		}

		public static Func<T2, T0> ConRL<T0, T1, T2>(this Func<T1, T0> a, Func<T2, T1> b)
		{
			return t0 => a(b(t0));
		}

		public static Func<T0, T2> ConLR<T0, T1, T2>(this Func<T0, T1> a, Func<T1, T2> b)
		{
			return t0 => b(a(t0));
		}
	}
}
