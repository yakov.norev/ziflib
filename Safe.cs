﻿using System;

namespace ZifLib
{
	public static class Safe
	{
		public static Exception A(Action a)
		{
			try
			{
				a();
				return null;
			}
			catch (Exception e)
			{
				return e;
			}
		}

		public static Exception As(params Action[] a)
		{
			try
			{
				foreach (var item in a) item();
				return null;
			}
			catch (Exception e)
			{
				return e;
			}
		}

		public static T F<T>(Func<T> a)
		{
			try
			{
				return a();
			}
			catch (Exception)
			{
				return default(T);
			}
		}

		public static T F<T, T1>(Func<T1, T> a, T1 p1)
		{
			try
			{
				return a(p1);
			}
			catch (Exception)
			{
				return default(T);
			}
		}

		public static void A(Action a, Action<Exception> aex)
		{
			try
			{
				a();
			}
			catch (Exception e)
			{
				aex(e);
			}

		}

		public static void F<T>(Func<T> a, Action<T> res, Action<Exception> aex)
		{
			try
			{
				res(a());
			}
			catch (Exception e)
			{
				aex(e);
			}

		}

		public static T F<T>(Func<T> a, Action<Exception> callback)
		{
			try
			{
				return a();
			}
			catch (Exception e)
			{
				callback(e);
				return default(T);
			}
		}

		public static T F<T, Tp1>(Func<Tp1, T> a, Action<Exception> callback, Tp1 p1)
		{
			try
			{
				return a(p1);
			}
			catch (Exception e)
			{
				callback(e);
				return default(T);
			}
		}

		public static Exception A(Delegate a, params object[] objs)
		{
			try
			{
				a.DynamicInvoke(objs);
				return null;
			}
			catch (Exception e)
			{
				return e;
			}
		}

		public static T F<T>(Delegate a, params object[] objs)
		{
			try
			{
				return (T) a.DynamicInvoke(objs);
			}
			catch (Exception)
			{
				return default(T);
			}
		}

		//public static T F<T>(Delegate a, Action<Exception> callback, params object[] objs)
		//{
		//	try
		//	{
		//		return (T)a.DynamicInvoke(objs);
		//	}
		//	catch (Exception e)
		//	{
		//		callback(e);
		//		return default(T);
		//	}
		//}
		public static Func<Exception> Da(Action a)
		{
			return () => A(a);
		}

		public static Func<T> Df<T>(Func<T> a)
		{
			return () => F(a);
		}

		public static void Ai(Action a)
		{
			try
			{
				a();
			}
			catch (Exception)
			{

			}
		}

		public static bool Mb(Action a)
		{
			a();
			return true;
		}

		public static void Mi(Action a)
		{
			a();
		}

		public static T Mi<T>(Func<T> a)
		{
			return a();
		}

		public static bool Ab(Action a)
		{
			try
			{
				a();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static Exception Ar(Action a)
		{
			try
			{
				a();
				return null;
			}
			catch (Exception e)
			{
				return e;
			}
		}

		public static void Aie(Action a, Action<Exception> ex_action)
		{
			try
			{
				a();
			}
			catch (Exception e)
			{
				ex_action(e);
			}
		}

		public static bool Abe(Action a, Action<Exception> ex_action)
		{
			try
			{
				a();
				return true;
			}
			catch (Exception e)
			{
				ex_action(e);
				return false;
			}
		}

		public static Exception Are(Action a, Action<Exception> ex_action)
		{
			try
			{
				a();
				return null;
			}
			catch (Exception e)
			{
				ex_action(e);
				return e;
			}
		}
		//----------------------------------------------------------------

		public static T Fi<T>(Func<T> a)
		{
			try
			{
				return a();
			}
			catch (Exception)
			{
				return default(T);
			}
		}

		public static bool Fbc<T>(Func<T> a, Action<T> c)
		{
			try
			{
				c(a());
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static Exception Frc<T>(Func<T> a, Action<T> c)
		{
			try
			{
				c(a());
				return null;
			}
			catch (Exception e)
			{
				return e;
			}
		}

		public static T Fie<T>(Func<T> a, Action<Exception> ex_action)
		{
			try
			{
				return a();
			}
			catch (Exception e)
			{
				ex_action(e);
				return default(T);
			}
		}

		public static bool Fbe<T>(Func<T> a, Action<Exception> ex_actio)
		{
			try
			{
				a();
				return true;
			}
			catch (Exception e)
			{
				ex_actio(e);
				return false;
			}
		}

		public static Exception Fre<T>(Func<T> a, Action<Exception> ex_actio)
		{
			try
			{
				a();
				return null;
			}
			catch (Exception e)
			{
				ex_actio(e);
				return e;
			}
		}

		public static T Fice<T>(Func<T> a, Action<T> c, Action<Exception> ex_actio)
		{
			try
			{
				T t = a();
				c(t);
				return t;
			}
			catch (Exception e)
			{
				ex_actio(e);
				return default(T);
			}
		}

		public static bool Fbce<T>(Func<T> a, Action<T> c, Action<Exception> ex_actio)
		{
			try
			{
				c(a());
				return true;
			}
			catch (Exception e)
			{
				ex_actio(e);
				return false;
			}
		}

		public static Exception Frce<T>(Func<T> a, Action<T> c, Action<Exception> ex_actio)
		{
			try
			{
				c(a());
				return null;
			}
			catch (Exception e)
			{
				ex_actio(e);
				return e;
			}
		}

		public static bool X(Action a, Action<Exception> ex_actio)
		{
			return Abe(a, ex_actio);
		}

		public static bool X<T>(Func<T> a, Action<T> c, Action<Exception> ex_actio)
		{
			return Fbce(a, c, ex_actio);
		}
	}
}
