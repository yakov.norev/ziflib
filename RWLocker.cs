﻿using System;
using System.Threading;
using ZifLib.Utils.Common;

namespace ZifLib
{
	public interface IRWLockable<T> : IRWLockable
	{
		new IRWLocker<T> Locker { get; }
	}

	public interface IRWLockable
	{
		IRWLocker Locker { get; }
	}

	public interface IRWLocker
	{
		void Read(Action a);
		Tout Read<Tout>(Func<Tout> a);
		void Write(Action a);
		Tout Write<Tout>(Func<Tout> a);
	}

	public interface IRWLocker<T> : IRWLocker
	{
		void Read(Action<T> a);
		Tout Read<Tout>(Func<T, Tout> a);
		void Write(Action<T> a);
		Tout Write<Tout>(Func<T, Tout> a);
	}

	public class RWLocker : IRWLocker
	{
		public static RWLocker<T> Create<T>(T obj) where T : class => new RWLocker<T>(obj);
		protected readonly ReaderWriterLock Lock = new ReaderWriterLock();
		public readonly TimeSpan timeout = TimeSpan.FromSeconds(5);

		public void Read(Action a)
		{
			try
			{
				Lock.AcquireReaderLock(timeout);
				a();
			}
			finally
			{
				Lock.ReleaseReaderLock();
			}
		}

		public Tout Read<Tout>(Func<Tout> a)
		{
			try
			{
				Lock.AcquireReaderLock(timeout);
				return a();
			}
			finally
			{
				Lock.ReleaseReaderLock();
			}
		}

		public void Write(Action a)
		{
			try
			{
				Lock.AcquireWriterLock(timeout);
				a();
			}
			finally
			{
				Lock.ReleaseWriterLock();
			}
		}

		public Tout Write<Tout>(Func<Tout> a)
		{
			try
			{
				Lock.AcquireWriterLock(timeout);
				return a();
			}
			finally
			{
				Lock.ReleaseWriterLock();
			}
		}
	}

	public class RWLocker<T> : RWLocker, IRWLocker<T> where T : class
	{
		public RWLocker(T obj)
		{
			this.obj = obj;
		}

		readonly T obj;

		public void Read(Action<T> a) => Read(() => a(obj));
		public Tout Read<Tout>(Func<T, Tout> a) => Read(() => a(obj));
		public void Write(Action<T> a) => Write(() => a(obj));
		public Tout Write<Tout>(Func<T, Tout> a) => Write(() => a(obj));
	}

	public class RWLockerFake : IRWLocker
	{
		public void Read(Action a) => a();
		public Tout Read<Tout>(Func<Tout> a) => a();
		public void Write(Action a) => a();
		public Tout Write<Tout>(Func<Tout> a) => a();
	}

	public class RWLockerOne : IRWLocker
	{
		public void Read(Action a)
		{
			this.Lock(a);
		}

		public Tout Read<Tout>(Func<Tout> a)
		{
			return this.Lock(a);
		}

		public void Write(Action a)
		{
			this.Lock(a);
		}

		public Tout Write<Tout>(Func<Tout> a)
		{
			return this.Lock(a);
		}
	}
}