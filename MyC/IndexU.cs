﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ZifLib.MyC.Interfaces;
using ZifLib.Utils.Collection;

namespace ZifLib.MyC
{
    public class IndexU<T, TI> : IIndexTypedUController<T, TI>
    {
        public IMCController<T> Coll { get; }
        public string Name { get; }

        protected readonly IDictionary<TI, int> hasher;
        protected readonly Func<T, TI> F;

        protected int? GetX(TI h) => hasher.GetNulleble(h);
        protected int? GetXObj(T x) => GetX(F(x));

        //ICollection<int> GetY(TI h) => GetX(h)?.CopyR() ?? new int[0];
        //ICollection<int> GetYObj(T x) => GetY(F(x));
        //protected  IList<int> GetCreate(TI x) => hasher.Create(x, () => new List<int>());		
        public void Clear() => hasher.Clear();

        public IndexU(IMCController<T> coll, string name, Func<T, TI> f, IEqualityComparer<TI> comparer = null)
        {
            Name = name;
            Coll = coll;
            F = f;
            hasher = new Dictionary<TI, int>(comparer ?? EqualityComparer<TI>.Default);
        }

        protected bool RemoveRequest(int? i)
        {
            if (i == null) return false;
            Coll.RemoveAt(i.Value);
            return true;
        }

        public void AddIndex(int index) => hasher[F(Coll[index])] = index;

        public void RemoveReplaceIndex(int newind, int oldind)
        {
            RemoveIndex(newind);
            ReplaceIndex(oldind, newind);
        }

        public void RemoveIndex(int ind)
        {
            Remove(F(Coll[ind]));
        }

        public void ReplaceIndex(int oldIndex, int newIndex)
        {
            if (oldIndex == newIndex) return;
            hasher[F(Coll[oldIndex])] = newIndex;
        }

        public bool Test(T a, int ind)
        {
            var v = hasher.GetNulleble(F(a));
            return v.HasValue && v != ind;
        }

        public bool Test(T a)
        {
            var v = hasher.GetNulleble(F(a));
            return v.HasValue;
        }

        public ICollection<TI> keys() => hasher.Keys;

        public ICollection<T> Get(TI x)
        {
            var t = Get1(x);
            if (t == null) return null;
            return new[] {t};
        }

        public IList<int> GetI(TI x)
        {
            var i = GetX(x);
            return i != null ? new[] {i.Value} : new int[0];
        }

        public ICollection<T> GetOfObj(T x) => Get(F(x));
        int IIndexTypedU<T, TI>.Index(TI x) => Coll.Locker.Read(() => GetX(x) ?? -1); //because threadsafe
        int? IIndexTypedU<T, TI>.Index_n(TI x) => Coll.Locker.Read(() => GetX(x)); //because threadsafe

        public T Get1(TI x) => Coll.Locker.Read(() =>
        {
            var v = GetX(x);
            if (v == null) return default(T);
            return Coll[v.Value];
        });

        public T GetOfObj1(T x) => Get1(F(x));

        public int Remove(TI x) => Remove1(x) ? 1 : 0;
        public int RemoveOfObj(T x) => Remove(F(x));
        public bool Remove1(TI x) => RemoveRequest(GetX(x));
        public bool RemoveOfObj1(T x) => Remove1(F(x));

        public void Index(TI x, Action<int> f) => Coll.Locker.Read(() =>
        {
            var f1 = GetX(x);
            if (f1.HasValue) f(f1.Value);
        });

        public bool GetCreate(TI x, Action<int> f, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = GetX(x);
            if (f1.HasValue)
            {
                var t = f1.Value;
                f(t);
                return true;
            }
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !F(t1).Equals(x)) Debugger.Break();
                Coll.Add(t1);
                f1 = GetX(x);
                if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return false;
            }
        });

        public T GetCreateR(TI x, Action<int> f, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = GetX(x);
            if (f1.HasValue)
            {
                var t = f1.Value;
                f(t);
                return Coll[t];
            }
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !F(t1).Equals(x)) Debugger.Break();
                Coll.Add(t1);
                //    f1 = GetX(x);
                //  if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return t1;
            }
        });

        public T GetCreate(TI x, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = GetX(x);
            if (f1.HasValue) return Coll[f1.Value];
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !x.Equals(F(t1))) Debugger.Break();
                Coll.Add(t1);
                // f1 = GetX(x);
                // if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return t1;
            }
        });

        public bool Unique => true;
    }
}
