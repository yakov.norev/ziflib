﻿using System.Collections.Generic;
using System.Linq;
using ZifLib.MyC.Interfaces;

namespace ZifLib.MyC
{
	public abstract class IndexBase<T, TI> : IIndexTypedController<T, TI>
	{
		protected IndexBase(IMCController<T> coll, string nam)
		{
			Name = nam;
			Coll = coll;
		}
		public IMCController<T> Coll { get; }
		public string Name { get; }

		protected T Ix(int a) => Coll[a];

		protected int RemoveRequest(IEnumerable<int> li_)
		{
			return Coll.RemoveAll(li_);
		}

		void IIndexController<T>.AddIndex(int index) => GetCreateI(index).Add(index);

		void IIndexController<T>.RemoveReplaceIndex(int newind, int oldind)
		{
			var controller = ((IIndexController<T>)this);
			controller.RemoveIndex(newind);
			controller.ReplaceIndex(oldind, newind);	
		}
		protected abstract TI F(T x);
		void IIndexController<T>.RemoveIndex(int ind)
		{
			var ObjTi = F(Ix(ind));
			var li = GetXObj(Ix(ind));
			if (li != null)
			{
				li.Remove(ind);

				if (li.Count == 0)
					Rem(ObjTi);
			}
		}
		void IIndexController<T>.ReplaceIndex(int oldIndex, int newIndex)
		{
			if (oldIndex == newIndex) return;
			var il = GetXObj(Ix(oldIndex));
			if (il != null) il[il.IndexOf(oldIndex)] = newIndex;
		}
		
		public abstract void Clear();
		protected abstract IList<int> GetX(TI h);
		protected abstract IList<int> GetXObj(T x);
		protected abstract IList<int> GetCreate(TI x);
		public ICollection<T> Get(TI x) => Coll.Locker.Read(() => GetX(x)?.Select(Ix)?.ToArray() ?? new T[0]);
		public abstract IList<int> GetI(TI x);
		public ICollection<T> GetOfObj(T x) => Coll.Locker.Read(() => GetXObj(x)?.Select(Ix)?.ToArray() ?? new T[0]);
		
		protected IList<int> GetCreateI(int x) => GetCreate(F(Coll[x]));
				
		public abstract int Remove(TI x);
		protected abstract void Rem(TI ObjTi);

		public abstract int RemoveOfObj(T x);
		public abstract ICollection<TI> keys();
		public abstract bool Unique { get; }
	}
}
