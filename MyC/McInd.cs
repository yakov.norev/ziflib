using System;
using System.Collections.Generic;
using System.Linq;
using ZifLib.MyC.Interfaces;

namespace ZifLib.MyC
{
    public partial class Mc<T>
    {
        private static int iIndex;

        static string NewNameForIndex() => "Index_" + (iIndex++); // App.Rnd.Next().ToString();

        IRelationTyped<T, T1> IIndexColl<T>.CreateRel<T1>(Func<T, IDictionary<T1, object>> col)
        {
            var r = new Relation<T, T1>(this, col, NewNameForIndex());
            refs[r.Name] = r;
            for (int i = 0; i < items.Count; i++) r.AddIndex(i);
            return r;
        }

        IIndexTypedU<T, T1> IIndexColl<T>.CreateU<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer, string name)
        {
            return Indexs.Create(new IndexU<T, T1>(this, name ?? NewNameForIndex(), col, comparer));
        }

        IIndexTyped<T, T1> IIndexColl<T>.Create<T1>(Func<T, T1> col, bool unique, IEqualityComparer<T1> comparer,
            string name)
            =>
                Indexs.Create(unique
                    ? (IIndexTypedController<T, T1>) new IndexU<T, T1>(this, name ?? NewNameForIndex(), col, comparer)
                    : new Index<T, T1>(this, name ?? NewNameForIndex(), col, null, comparer));

        IIndexTyped<T, T1> IIndexColl<T>.CreateI<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer, string name)
            =>
                Indexs.Create(new Index<T, T1>(this, name ?? NewNameForIndex(), col, null, comparer));

        public IIndexTyped<T, T1> CreateIc<T1>(Func<T, T1> col, Func<T1, IList<int>> cr,
            IEqualityComparer<T1> comparer = null, string name = null)
            =>
                Indexs.Create(new Index<T, T1>(this, name ?? NewNameForIndex(), col, cr, comparer));

        IIndexTyped<T, T1> IIndexColl<T>.Create<T1>(IIndexTypedController<T, T1> n)
        {
            locker.Write(() =>
            {
                indexs[n.Name] = n;
                for (int i = 0; i < items.Count; i++) n.AddIndex(i);
            });
            return n;
        }

        IIndexTypedU<T, T1> IIndexColl<T>.Create<T1>(IIndexTypedUController<T, T1> n)
        {
            locker.Write(() =>
            {
                indexs[n.Name] = n;
                for (int i = 0; i < items.Count; i++) n.AddIndex(i);
            });
            return n;
        }

        public IIndexColl<T> Indexs => this;
        IIndex<T> IIndexColl<T>.this[string key] => locker.Read(() => indexs[key]);

        IEnumerator<IIndex<T>> IEnumerable<IIndex<T>>.GetEnumerator() =>
            locker.Read(() => indexs.Values.Cast<IIndex<T>>().GetEnumerator());
    }
}
