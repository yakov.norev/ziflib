﻿using System;
using System.Collections.Generic;
using ZifLib.MyC.Interfaces;
using ZifLib.Utils.Collection;

namespace ZifLib.MyC
{
	public class Index<T, TI> : IndexBase<T, TI>, IIndexTypedController<T, TI>
	{
		private readonly IDictionary<TI, IList<int>> hasher;
		private readonly Func<T, TI> f;
		private readonly Func<TI, IList<int>> _creator;

		public override ICollection<TI> keys() => hasher.Keys;
		public override bool Unique => false;
		public override IList<int> GetI(TI x) => hasher.Get(x);
		public override int Remove(TI x) => Coll.Locker.Write(() => RemoveRequest(GetX(x)));
		public override int RemoveOfObj(T x) => Remove(f(x));

		protected override IList<int> GetX(TI h) => hasher.Get(h, null);
		protected override IList<int> GetXObj(T x) => GetX(f(x));

		ICollection<int> GetY(TI h) => GetX(h)?.CopyR() ?? new int[0];
		ICollection<int> GetYObj(T x) => GetY(f(x));
		protected override IList<int> GetCreate(TI x) => hasher.Create(x, _creator);

		protected override TI F(T x) => f(x);
		public override void Clear() => hasher.Clear();

		protected override void Rem(TI ObjTi) => hasher.Remove(ObjTi);

		public Index(IMCController<T> coll, string name, Func<T, TI> f, Func<TI, IList<int>> creator = null, IEqualityComparer<TI> comparer = null)
			: base(coll, name)
		{
			this.f = f;
			hasher = new Dictionary<TI, IList<int>>(comparer ?? EqualityComparer<TI>.Default);
			_creator = creator ?? (_ => new List<int>());
		}
	}
}
