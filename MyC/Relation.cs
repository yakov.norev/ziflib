﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ZifLib.MyC.Interfaces;
using ZifLib.Utils.Collection;
using ZifLib.Utils.Common;

namespace ZifLib.MyC
{
    public class Relation<T, TI> : IRelationTypedController<T, TI>
    {
        readonly IDictionary<TI, int> g = new Dictionary<TI, int>();

        public IMCController<T> Coll { get; }
        public Func<T, IDictionary<TI, object>> F { get; }
        public string Name { get; }

        public Relation(IMCController<T> coll, Func<T, IDictionary<TI, object>> f, string name = null)
        {
            Coll = coll;
            F = f;
            Name = name;
        }
        public void AddIndex(int index) => F(Coll[index]).Keys.For(a => g[a] = index);
        public int Index(TI a) => g.Get(a, -1);
        public T Get(TI x) => Coll.Locker.Read(() =>
        { var i = Index(x); return i < 0 ? default(T) : Coll[i]; });
        public T Create(TI x, Func<T> gener) => Coll.Locker.Write(() =>
        {
            var i = g.GetNulleble(x);
            if (i != null) return Coll[i.Value];
            var gg = gener();
            Coll.Add(gg);
            return gg;
        });

        public bool Remove(TI x, bool UpCascade = false)
        {
            if (!g.ContainsKey(x)) return false;
            var uuu = g.Get(x);
            var hhh = F(Coll[uuu]);
            hhh.Remove(x);
            if (UpCascade && hhh.Count == 0)
            {
                Coll.RemoveAt(uuu);
            }
            return g.Remove(x);
        }

        void IRelationController<T>.Clear() => g.Clear();

        public void RemoveReplaceIndex(int newind, int oldind)
        {
            RemoveIndex(newind);
            ReplaceIndex(oldind, newind);
        }
        public void ReplaceIndex(int oldind, int newind) => F(Coll[oldind]).Keys.Ё(a => Test(oldind, a)).For(a => g[a] = newind);
        public void RemoveIndex(int ind) => F(Coll[ind]).Keys.Ё(a => Test(ind, a)).For(a => g.Remove(a));

        private void Test(int ind, ICollection<TI> ff)
        {
            if (Debugger.IsAttached)
            {
                if (ff.Any(a => g[a] != ind)) Debugger.Break();
            }
        }

        public void Index(TI x, Action<int> f)
        {
            var i = Index(x);
            if (i >= 0) f(i);
        }
    }
}
//	public int RemoveOfObj(T x) => Remove(F(x));

//		protected IList<int> GetX(TI h) => new int?[] { g.Get_n(h) }.Where(a => a.HasValue).Select(a => a.Value).ToArray();
//	protected IList<int> GetX(IEnumerable<string> h) => h.SelectMany(a => GetX(a)).ToList();

//protected void Rem(IEnumerable<string> ObjTi)
//{
//	ObjTi.For(a => { var gg = g[a]; jj.Remove(gg); g.Remove(a); });
//}
//protected T Ix(int a) => Coll[a];