﻿using System.Collections.Generic;

namespace ZifLib.MyC.Interfaces
{
    public interface IMCController<T> : IRWLockable, IList<T>
	{
		int RemoveAll(IEnumerable<int> li);
	}
}
