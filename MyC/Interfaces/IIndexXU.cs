﻿namespace ZifLib.MyC.Interfaces
{
    public interface IIndexXU<T> : IIndex<T>
	{
		T GetOfObj1(T x);
		bool RemoveOfObj1(T x);
	}
}
