﻿using System;
using System.Collections.Generic;

namespace ZifLib.MyC.Interfaces
{
    public interface IIndexColl<T> : IEnumerable<IIndex<T>>
	{
		IIndex<T> this[string key] { get; }
		IIndexTypedU<T, T1> CreateU<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer = null, string name = null);

		IIndexTyped<T, T1> Create<T1>(Func<T, T1> col, bool unique = false, IEqualityComparer<T1> comparer = null,
			string name = null);

		IIndexTyped<T, T1> Create<T1>(IIndexTypedController<T, T1> ind);
		IRelationTyped<T, T1> CreateRel<T1>(Func<T, IDictionary<T1, object>> col);
        IIndexTypedU<T, T1> Create<T1>(IIndexTypedUController<T, T1> n);
        IIndexTyped<T, T1> CreateI<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer = null, string name = null);
        IIndexTyped<T, T1> CreateIc<T1>(Func<T, T1> func, Func<T1, IList<int>> cr, IEqualityComparer<T1> comparer = null, string name = null);
	}
}
