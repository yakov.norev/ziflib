﻿using System;


namespace ZifLib.MyC.Interfaces
{
	/*=============================================*/
	public interface IRelation
	{
		string Name { get; }
	}
	public interface IRelation<T> : IRelation
	{
		//	ICollection<T> GetOfObj(T x);
		//	bool RemoveOfObj(T x);
	}
	public interface IRelationTyped<T, T1> : IRelation<T>
	{
		T Create(T1 x, Func<T> generator);
		T Get(T1 x);
		bool Remove(T1 x, bool UpCascade = false);
		void Index(T1 x, Action<int> f);
		void AddIndex(int c);
	}
	public interface IRelationController<T> : IRelation<T>
	{
		IMCController<T> Coll { get; }
		void AddIndex(int c);
		void Clear();
		void RemoveReplaceIndex(int newind, int oldind);		
		void ReplaceIndex(int oldind, int newind);
		void RemoveIndex(int ind);
	}
	public interface IRelationTypedController<T, T1> : IRelationController<T>, IRelationTyped<T, T1>
	{
	}
}
