﻿using System;

namespace ZifLib.MyC.Interfaces
{
    public interface IIndexTypedU<T, TI> : IIndexXU<T>, IIndexTyped<T, TI>
	{
		T Get1(TI x);
		bool Remove1(TI x);
		bool GetCreate(TI x, Action<int> f, Func<T> g);
		T GetCreateR(TI x, Action<int> f, Func<T> g);
		T GetCreate(TI x, Func<T> g);
		void Index(TI x, Action<int> f);
		int Index(TI x); //because threadsafe
		int? Index_n(TI x); //because  threadsafe
	}
}
