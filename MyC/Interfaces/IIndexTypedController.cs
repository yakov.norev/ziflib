﻿namespace ZifLib.MyC.Interfaces
{
    public interface IIndexTypedController<T, TI> : IIndexController<T>, IIndexTyped<T, TI>
	{
	}
}
