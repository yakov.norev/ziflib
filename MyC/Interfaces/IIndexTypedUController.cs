﻿namespace ZifLib.MyC.Interfaces
{
    public interface IIndexTypedUController<T, TI> : IIndexTypedController<T, TI>, IIndexControllerXU<T>,
		IIndexTypedU<T, TI>
	{

	}
}
