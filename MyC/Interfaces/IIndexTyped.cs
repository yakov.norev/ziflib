﻿using System.Collections.Generic;

namespace ZifLib.MyC.Interfaces
{
    public interface IIndexTyped<T, T1> : IIndex<T>
	{
		ICollection<T1> keys();
		ICollection<T> Get(T1 x);
		IList<int> GetI(T1 x);
        int Remove(T1 x);
	}
}
