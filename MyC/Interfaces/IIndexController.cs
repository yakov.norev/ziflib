﻿namespace ZifLib.MyC.Interfaces
{
    public interface IIndexController<T> : IIndex<T>
	{
		IMCController<T> Coll { get; }
		void AddIndex(int c);
		void Clear();
		void RemoveReplaceIndex(int newind, int oldind);
		void ReplaceIndex(int oldind, int newind);
		void RemoveIndex(int ind);
	}
}
