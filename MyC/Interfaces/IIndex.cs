﻿using System.Collections.Generic;

namespace ZifLib.MyC.Interfaces
{
    public interface IIndex : IRelation
	{
		bool Unique { get; }
	}

	public interface IIndex<T> : IIndex
	{
		ICollection<T> GetOfObj(T x);
		int RemoveOfObj(T x);
	}
}
