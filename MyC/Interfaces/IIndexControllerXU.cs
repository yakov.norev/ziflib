﻿namespace ZifLib.MyC.Interfaces
{
    public interface IIndexControllerXU<T> : IIndexController<T>, IIndexXU<T>
    {
		bool Test(T a, int ind);
		bool Test(T a);
    }
}
