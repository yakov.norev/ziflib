﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ZifLib.MyC.Interfaces;
using ZifLib.Utils.Collection;

namespace ZifLib.MyC
{
    public class Mc
    {
        public static Mc<T> Create<T>(T prototype) => new Mc<T>();
        public static Mc<T> CreateList<T>(IList<T> a) => new Mc<T>(a);
    }
    public partial class Mc<T> : IList<T>, IList, ICollection, IIndexColl<T>, IMCController<T>
    {
        protected readonly IList<T> items;
        private readonly IDictionary<string, IIndexController<T>> indexs = new Dictionary<string, IIndexController<T>>();
        private readonly IDictionary<string, IRelationController<T>> refs = new Dictionary<string, IRelationController<T>>();

        private readonly IRWLocker locker;
        public Mc(IList<T> items = null, bool Multithread = false)
        {
            locker = Multithread ? (IRWLocker)new RWLockerOne() : new RWLockerFake();
            this.items = items ?? new List<T>();
        }
        public void Add(T item) => AssertKey(AddTry(item));
        public void Insert(int index, T item) => AssertKey(InsertTry(index, item));
        public void Update(int index, T item) => AssertKey(UpdateTry(index, item));

        public void Clear() => locker.Read(() =>
        {
            indexs.Values.For(r => r.Clear());
            refs.Values.For(r => r.Clear());
            items.Clear();
        });

        public bool Remove(T item) => locker.Write(() =>
        {
            var ind = IndexOf(item);
            if (ind < 0) return false;
            RemoveAt(ind);
            return true;
        });
        public virtual bool AddTry(T item) => locker.Write(() =>
        {
            if (Test(item)) return false;
            var index = items.Count;
            items.Add(item);
            UpdateIndex(index);
            return true;
        });

        public void UpdateIndex(int c) => locker.Read(() =>
        {
            foreach (var inde in indexs.Values) inde.AddIndex(c);
            foreach (var inde in refs.Values) inde.AddIndex(c);
        });
        public void UpdateIndex(int c, Action<T> a) => locker.Read(() =>
        {
            a(this[c]);
            foreach (var inde in indexs.Values) inde.AddIndex(c);
            foreach (var inde in refs.Values) inde.AddIndex(c);
        });

        /**
		 * вставка производится с переносом элемента по текущему индексу в конец		 
		 */
        public bool InsertTry(int index, T item) => locker.Write(() =>
        {
            if (Test(item)) return false;
            foreach (var il in indexs.Values) il.ReplaceIndex(index, Count);
            foreach (var il in refs.Values) il.ReplaceIndex(index, Count);
            items.InsertFast(index, item);
            UpdateIndex(index);
            return true;
        });

        public bool UpdateTry(int index, T value) => locker.Write(() =>
        {
            if (ReferenceEquals(items[index], value)) return true;
            if (Test(index, value)) return false;

            foreach (var inde in indexs.Values) inde.RemoveIndex(index);
            foreach (var inde in refs.Values) inde.RemoveIndex(index);
            items[index] = value;
            UpdateIndex(index);
            return true;
        });

        /**
		 * удаление производится с переносом элемента с конца на текущий индекс
		 */
        public virtual void RemoveAt(int ind) => locker.Write(() =>
        {
            var indlast = items.Count - 1;
            if (ind != indlast)
            {
                indexs.Values.For(il => il.RemoveReplaceIndex(ind, indlast));
                refs.Values.For(r => r.RemoveReplaceIndex(ind, indlast));
            }
            items.RemoveFast(ind);
        });

        public virtual int RemoveAll(IEnumerable<int> _ind) => locker.Write(() =>
        {
            if (_ind == null) return 0;

            var list = _ind.Distinct().ToList();
            list.Sort((b, a) => a - b);
            foreach (var ind in list)
            {
                var indlast = items.Count - 1;
                if (ind != indlast)
                {
                    indexs.Values.For(il => il.RemoveReplaceIndex(ind, indlast));
                    refs.Values.For(r => r.RemoveReplaceIndex(ind, indlast));
                }
                items.RemoveFast(ind);
            }

            return list.Count;
        });

        

        public T this[int index] { get => locker.Read(() => items[index]); set => Update(index, value); }

        IEnumerable<IIndexControllerXU<T>> indUs => indexs.Values.OfType<IIndexControllerXU<T>>();
        /* проверка для замены 
		 * существует ли объект сопадающий по уникальным индексам
		 * на отличающийся по местоположению от вставляемого
		 */
        private bool Test(int index, T value) => indUs.Any(ind => ind.Test(value, index));
        private bool Test(T item) => indUs.Any(i => i.Test(item));
        private void AssertKey(bool b = false)
        {
            if (!b) throw new ArgumentException();
        }

        public IEnumerator<T> GetEnumerator() => locker.Read(() => items.GetEnumerator());
        public bool Contains(T item) => locker.Read(() => items.Contains(item));
        public void CopyTo(T[] array, int arrayIndex) => locker.Read(() => items.CopyTo(array, arrayIndex));
        public int IndexOf(T item) => locker.Read(() => items.IndexOf(item));
        public void CopyTo(Array array, int index) => locker.Read(() => ((ICollection)items).CopyTo(array, index));
        public int Count => locker.Read(() => items.Count);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int Add(object value)
        {
            Add((T)value);
            return Count - 1;
        }
        public bool Contains(object value) => (value is T a) ? Contains(a) : false;
        public int IndexOf(object value) => (value is T a) ? IndexOf(a) : -1;
        public void Insert(int index, object value) => Insert(index, (T)value);
        public void Remove(object value) => Remove((T)value);

        public bool IsReadOnly => false;
        object ICollection.SyncRoot => locker;// :-)
        bool ICollection.IsSynchronized => true;
        IRWLocker IRWLockable.Locker => locker;

        public bool IsFixedSize => false;

        object IList.this[int index] { get => this[index]; set => this[index] = (T)value; }
    }
}
